from game import Game

def main():
    game = Game()
    game.run_new_game()


if __name__ == '__main__':
    main()