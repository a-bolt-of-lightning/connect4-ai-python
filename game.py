import numpy as np

COLs = 7
ROWs = 6
CONNECT_N = 4

PL1 = '1'
PL2 = '2'

class Game:

    def get_next_turn(self, current_turn):
        if current_turn == PL1:
            return PL2
        else:
            return PL1

    def drop_unto_board(self, board, piece, col):
        if(self.is_col_filled(board, col)):
            return False
        for i in range(ROWs):
            if  board[i][col] == ' ':
                board[i][col] = piece
                return True

    def is_col_filled(self, board, col):
        return not board[ROWs-1][col] == ' '

    def check_win(self, board, piece):
        #horizontal
        for r in range(0,ROWs):
            for c in range(0, COLs-3):
                res = True
                for i in range(0, CONNECT_N):
                        res = res and board[r][c+i] == piece

                if res:
                    return True

        #vertical
        for r in range(0, ROWs-3):
            for c in range(0, COLs):
                res = True
                for i in range(0, CONNECT_N):
                    res = res and board[r + i][c] == piece

                if res:
                    return True

        #main diag
        for r in range(0, ROWs-3):
            for c in range(0, COLs-3):
                res = True
                for i in range(0, CONNECT_N):
                    res = res and board[r + i][c + i] == piece

                if res:
                    return True

        #other diag
        for r in range(3, ROWs):
            for c in range(0, COLs-3):
                res = True
                for i in range(0, CONNECT_N):
                    res = res and board[r - i][c+ i] == piece

                if res:
                    return True

    def print_board(self, board):
        print(board)

    def create_board(self):
        board = np.full((6, 7), ' ')
        return board

    def run_new_game(self):
        board = self.create_board()
        turn = PL1
        game_over = False

        while(not game_over):
            self.print_board(board)

            #get pl(turn) input
            selected_col = int(input(turn + ": Enter a column number (0-6): "))
            #call some func to put on board
            col_is_not_full = self.drop_unto_board(board, turn, selected_col)
            #also check for move validity n' stuffs
            if(self.check_win(board, turn)):
                print("end")
                break
            if(not col_is_not_full):
                print("col is full")
            #switch turn
            turn = self.get_next_turn(turn)
